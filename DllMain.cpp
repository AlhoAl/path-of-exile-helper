/*
	* This code can and will include bad practices, typos, broken/bad code and potential vulnerabilities, this is only meant to be used as a reference, nothing else.
	
	* Injectable DLL that will disable FoV limits for the game Path of Exile
	*	1.	Use pattern scanning to locate 2 different addresses in virtual memory, these addresses should hold the instructions used to restrict changing the in-game viewport's field of view.
	*	2.	Patch these addresses with new set of instruction(s) to allow us increase/decrease the FoV as we like.
	
	* Made by Alex @ Altonic/AAC in probably 2017-2018
*/

#include "Memory.h"
#include <string>

#define PROCESS_NAME "PathOfExile_x64.exe"
cMemory Memory(PROCESS_NAME);

WNDPROC oldWindowProc = 0;

LRESULT __stdcall Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_SYSKEYDOWN: //Alt down
		if (wParam == 0x51) //Q 0x51
			ExitProcess(0x69); //Close process, was used as a killswitch in the hardcore gamemode
		break;

	default:
		break;
	}

	return CallWindowProc((WNDPROC)oldWindowProc, hWnd, uMsg, wParam, lParam);
}

void HackerKnownAs4Chan() {
	while (1) {
		DWORD OldProtect = 0;

		ULONG_PTR ProtectSize = Memory.Signatures.ulPosFOVScale - Memory.Signatures.ulNegFOVScale;

		VirtualProtect((LPVOID)Memory.Signatures.ulPosFOVScale, ProtectSize, PAGE_EXECUTE_READWRITE, &OldProtect); //Change page protection
		memcpy((void*)Memory.Signatures.ulPosFOVScale, &Memory.Signatures.bytes_NewPosFOVScale, 1); //Replace old instructions with ours
		memcpy((void*)Memory.Signatures.ulNegFOVScale, &Memory.Signatures.bytes_NewNegFOVScale, 1);	//Replace old instructions with ours
		VirtualProtect((LPVOID)Memory.Signatures.ulPosFOVScale, ProtectSize, OldProtect, &OldProtect); //Switch back to the old protection value

		Sleep(1000);
	}
}

void Init() {
	Sleep(2000);

	//Get a handle to Path of Exile's window, which is used to "hook" its WindowProc
	HWND hWindow = FindWindow("POEWindowClass", 0);
	if (!hWindow) {
		MessageBox(0, "FindWindow failed, continuing without window proc hook.", std::to_string(GetLastError()).c_str(), 0);
	}
	else {
		//Replace old window procedure with ours, oldWindowProc will be the previous procedure if function was successful
		oldWindowProc = (WNDPROC)SetWindowLongPtr(hWindow, GWLP_WNDPROC, (LONG_PTR)&Hooked_WndProc);
		if (!oldWindowProc) {
			MessageBox(0, "SetWindowLongPtr failed, continuing without window proc hook.", std::to_string(GetLastError()).c_str(), 0);
		}
	}

	//Retrieve all loaded modules within the current process.
	// EDIT(01.02.2021): We could retrieve DOS/NT headers of the target module and get the base address that way, instead of doing all this, but an example of this is in the FindPattern function.
	MODULEENTRY32 modEntry;
	modEntry.dwSize = sizeof(MODULEENTRY32);
	
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessId(GetCurrentProcess()));

	if (!Module32First(snapshot, &modEntry)) {
		MessageBox(0, "Module32First failed.", 0, 0);
		CloseHandle(snapshot);
		return;
	}
	else {
		if (!strcmp(modEntry.szModule, PROCESS_NAME)) { //Check if first found module is the one we are looking for
			CloseHandle(snapshot); //Found the module we were looking for
		}
		else {
			while (Module32Next(snapshot, &modEntry)) { // Module32First didn't get us the right module, check the rest
				if (!strcmp(modEntry.szModule, PROCESS_NAME)) {
					CloseHandle(snapshot); //Found the module we were looking for
					break;
				}
			}
		}
	}

	Memory.Signatures.ulNegFOVScale = Memory.FindPattern(Memory.Signatures.sig_NegFOVScale);
	if (!Memory.Signatures.ulNegFOVScale) {
		MessageBox(0, "ulNegFOVScale was NULL after pattern scan", 0, 0);
		return;
	}
	Memory.Signatures.ulNegFOVScale += 2; //Signature is too short, add 2 bytes to reach the instructions we were looking for

	Memory.Signatures.ulPosFOVScale = Memory.FindPattern(Memory.Signatures.sig_PosFOVScale);
	if (!Memory.Signatures.ulPosFOVScale) {
		MessageBox(0, "ulPosFOVScale was NULL after pattern scan.", 0, 0);
		return;
	}
	Memory.Signatures.ulPosFOVScale += 2; //Signature is too short, add 2 bytes to reach the instructions we were looking for

	HackerKnownAs4Chan();
}

BOOL WINAPI DllMain(HMODULE hMod, DWORD dwReason, LPVOID lpvReserved) {
	UNREFERENCED_PARAMETER(lpvReserved);	//To supress warning C4100
	UNREFERENCED_PARAMETER(hMod);			//To supress warning C4100

	if (dwReason == DLL_PROCESS_ATTACH) {
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Init, 0, 0, 0); // If dll has been loaded into a process, create a thread in Init()
	}

	return 1;
}