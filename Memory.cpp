#include "Memory.h"
#include <string>


#define INRANGE(x,a,b)    (x >= a && x <= b) 
#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))

cMemory::cMemory(const char* szpModule) {
	szpModuleName = szpModule;
}

HMODULE cMemory::GetModuleHandleWait() {
	HMODULE hModule = NULL;

	while (!hModule) {
		hModule = GetModuleHandle(szpModuleName);
		Sleep(500);
	}

	return hModule;
}

ULONG_PTR cMemory::FindPattern(const char* szpPattern) {
	HMODULE hModule = GetModuleHandleWait();
	PIMAGE_DOS_HEADER pDOSHeader = (PIMAGE_DOS_HEADER)hModule;
	PIMAGE_NT_HEADERS pNTHeaders = (PIMAGE_NT_HEADERS)(((ULONG_PTR)hModule) + pDOSHeader->e_lfanew);

	const char* pat = szpPattern;
	ULONG_PTR firstMatch = NULL;
	for (ULONG_PTR pCur = ((ULONG_PTR)hModule) + pDOSHeader->e_lfanew; pCur < ((ULONG_PTR)hModule) + pNTHeaders->OptionalHeader.SizeOfCode; pCur++)
	{
		if (!*pat) return firstMatch;
		if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat)) {
			if (!firstMatch) firstMatch = pCur;
			if (!pat[2]) {
				return firstMatch;
			}
 			if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;
			else pat += 2;
		}
		else {
			pat = szpPattern;
			firstMatch = 0;
		}
	}

	return NULL;
}