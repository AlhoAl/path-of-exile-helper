#pragma once

#include <Windows.h>
#include <TlHelp32.h>

class cMemory
{
public:
	cMemory(const char* szpModule);

	HMODULE GetModuleHandleWait();
	ULONG_PTR FindPattern(const char* szpPattern);

	struct stSigs
	{
		//Signatures and opcodes
		const char* sig_NegFOVScale = "0F 2F C1 72 10 F3 0F 10 15 ? ? ? ? 0F 28 C8 0F 2F C2"; // + 2
		BYTE bytes_OrigNegFOVScale[1] = { 0xC1 };
		BYTE bytes_NewNegFOVScale[1] = { 0xC0 };

		//10 bytes in between, we wouldn't even need both of these signatures, we could just add the difference in bytes to the first one in order to get the second address to patch

		const char* sig_PosFOVScale = "0F 2F C2 77 03 0F 28 D1 F3 ? ? ? ? ? ? ? C6 07 01 48 8B 74 24 40"; // + 2
		BYTE bytes_OrigPosFOVScale[1] = { 0xC2 };
		BYTE bytes_NewPosFOVScale[1] = { 0xC0 };

		//Addresses
		ULONG_PTR ulPosFOVScale = 0;
		ULONG_PTR ulNegFOVScale = 0;
	}Signatures;

private:
	const char* szpModuleName = ""; //Init by constructor
};